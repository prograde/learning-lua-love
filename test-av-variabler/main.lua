function love.load()
  red = 0.5
  green = 0.5
  blue = 0.5
  width, height, flags = love.window.getMode()
end

function love.draw()
  love.graphics.setColor(red,green,blue,1)
  love.graphics.rectangle("fill",0,0,width, height);
end

function love.update()
  if love.keyboard.isDown('up') then
    if green < 1 then 
      green = green + 0.1
    end 
  end

  if love.keyboard.isDown('down') then
    if green > 0 then 
      green = green - 0.1
    end
  end

  if love.keyboard.isDown('pagedown') then
    if blue < 1 then 
      blue = blue + 0.1
    end 
  end

  if love.keyboard.isDown('pageup') then
    if blue > 0 then 
      blue = blue - 0.1
    end
  end

  if love.keyboard.isDown('left') then
    if red < 1 then 
      red = red + 0.1
    end 
  end

  if love.keyboard.isDown('right') then
    if red > 0 then 
      red = red - 0.1
    end
  end


  love.timer.sleep(0.2)
end

