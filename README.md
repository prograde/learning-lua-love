# Learning Game Development in Lua and Löve

## Installing Lua and Löve in Linux/Debian
```
sudo add-apt-repository ppa:bartbes/love-stable
sudo apt update
sudo apt install lua5.3 love
```

## Run games
After downloading or cloning the repo, in the main directory, run e.g.
`love pong`

Copyright (c) 2020 Prograde, Jon Johnson, released under GPLv3, see LICENSE.txt
