function love.load()
  -- set ball start position, angle and speed
  x, y = 300, 200
  vinkel = 45
  fart = 5

  -- set catcher position
  bx = 450

  -- game is on!
  alive = true
end

function love.draw()
  if alive then
    -- print coordinates
    love.graphics.setColor(0.8, 0.8, 1)
    love.graphics.print("x: "..math.floor(x).." y: "..math.floor(y).." v: "..vinkel , 20, 20)
    love.graphics.print("bx: "..math.floor(bx), 700, 20)

    -- draw ball
    love.graphics.setColor(1, 0.2, 0);
    love.graphics.circle("fill", x, y, 10)
  
    -- draw catcher
    love.graphics.setColor(0, 0.64, 0.64)
    love.graphics.rectangle("fill", bx, 500, 50, 10)
  else
    -- print game over
    love.graphics.setColor(0.2, 0.6, 1)
    love.graphics.rectangle("fill", 100, 100, 600, 300)
    love.graphics.setColor(0, 0, 0.1)
    love.graphics.print("Game Over", 260, 200, 0, 4)
  end
end

function love.update(dt)
  -- read left and right arrow keys
  if love.keyboard.isDown('left') then
    bx = bx - 8
  end
  if love.keyboard.isDown('right') then
    bx = bx + 8
  end

  -- calculate ball position
  x = x + fart * math.cos(math.rad(vinkel))
  y = y + fart * math.sin(math.rad(vinkel))

  -- bounce on walls
  if x < 10 then vinkel = 180 - vinkel end
  if y < 10 then vinkel = 360 - vinkel end
  if x > 760 then vinkel = 180 - vinkel end

  -- detect if catcher touches ball
  if y > 500 and x > bx and x < bx + 50 then vinkel = 360 - vinkel end

  -- detect if ball falls down
  if y > 600 then alive = false end
end

